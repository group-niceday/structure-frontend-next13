export interface ValidateConfig {
  ORDER           : ValidateEnum[];
  MESSAGES        : Message;
  VALIDATE_MESSAGE: ValidateMessage;
}

export enum ValidateEnum {
  isNotEmpty       = 'isNotEmpty',
  arrayNotEmpty    = 'arrayNotEmpty',

  isString         = 'isString',
  isNumber         = 'isNumber',
  isBoolean        = 'isBoolean',
  isNumberString   = 'isNumberString',
  isArray          = 'isArray',
  isDateString     = 'isDateString',
  isLocalDate      = 'isLocalDate',
  isLocalDateTime  = 'isLocalDateTime',
  isLocalTime      = 'isLocalTime',
  isHexColor       = 'isHexColor',
  isEnum           = 'isEnum',

  isRelativeUrl    = 'isRelativeUrl',
  isAbsoluteUrl    = 'isAbsoluteUrl',

  length           = 'length',
  max              = 'max',
  min              = 'min',
  minLength        = 'minLength',
  maxLength        = 'maxLength',
  isValid          = 'isValid',
  isNotValid       = 'isNotValid',
  minLocalDateTime = 'minLocalDateTime',
}

export type Message = {
  [K in ValidateEnum]: string;
};

export type ValidateMessage = {
  [K in string]: string;
};

export const VALIDATE_CONFIG: ValidateConfig = {
  ORDER: [
    ValidateEnum.isNotEmpty,

    ValidateEnum.isString,
    ValidateEnum.isNumber,
    ValidateEnum.isBoolean,
    ValidateEnum.isNumberString,
    ValidateEnum.isArray,
    ValidateEnum.isDateString,
    ValidateEnum.isLocalDate,
    ValidateEnum.isLocalDateTime,
    ValidateEnum.isLocalTime,
    ValidateEnum.isHexColor,
    ValidateEnum.isEnum,

    ValidateEnum.arrayNotEmpty,

    ValidateEnum.isRelativeUrl,
    ValidateEnum.isAbsoluteUrl,

    ValidateEnum.max,
    ValidateEnum.min,
    ValidateEnum.length,
    ValidateEnum.minLength,
    ValidateEnum.maxLength,

    ValidateEnum.isValid,
    ValidateEnum.isNotValid,
    ValidateEnum.minLocalDateTime,
  ],
  MESSAGES: {
    isNotEmpty      : '필수 정보 입니다.',

    isBoolean       : '자료형이 참거짓 형태여야 합니다.',
    isString        : '자료형이 문자 형태여야 합니다.',
    isNumber        : '자료형이 숫자 형태여야 합니다.',
    isNumberString  : '자료형이 숫자문자 형태여야 합니다.',
    isArray         : '자료형이 배열 형태여야 합니다.',
    isDateString    : '날짜 형식이어야 합니다.',
    isLocalDate     : '자료형이 년월일 형태여야 합니다.',
    isLocalDateTime : '자료형이 년월일시분초 형태여야 합니다.',
    isLocalTime     : '자료형이 시분초 형태여야 합니다.',
    isHexColor      : '자료형이 색상 형태여야 합니다.',
    isEnum          : '자료형이 코드 형태여야 합니다.',

    arrayNotEmpty   : '배열에 데이터가 필수 정보 입니다.',

    isAbsoluteUrl   : 'URL은 "http://", "https://"를 포함하고 있어야 합니다.',
    isRelativeUrl   : 'URL은 영문소문자, 숫자, _(underline), -(hyphen), .(period)으로만 구성되어야합니다.',

    max             : '최대값($constraint1)을 초과하였습니다.',
    min             : '최소값($constraint1) 이상이어야 합니다.',
    length          : '입력 범위($constraint1자 ~ $constraint2자) 사이를 입력하세요.',
    minLength       : '최소 입력 범위($constraint1) 이상이어야 합니다.',
    maxLength       : '최대 입력 범위($constraint1)를 초과하였습니다.',

    isValid         : '유효성 검사에 실패했습니다.',
    isNotValid      : '유효성 검사에 실패했습니다.',
    minLocalDateTime: 'minLocalDateTime',
  },
  VALIDATE_MESSAGE: {
    isEnumNotEquals: '올바른 코드가 아닙니다.',
  },
};
