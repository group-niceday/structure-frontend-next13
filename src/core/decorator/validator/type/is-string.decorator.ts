import { ValidationOptions                  } from 'class-validator';
import { IsString as ClassValidatorIsString } from 'class-validator';

import { VALIDATE_CONFIG                    } from '@/core/config/validate.config';

export function IsString(validationOptions?: ValidationOptions) {

  return (target: any, propertyKey: string) => {
    validationOptions = {message: VALIDATE_CONFIG.MESSAGES.isString, ...validationOptions ?? {}};

    ClassValidatorIsString(validationOptions)(target, propertyKey);
  };
}
