import { ValidationOptions                      } from 'class-validator';
import { IsDateString as ClassValidatorIsString } from 'class-validator';

import { VALIDATE_CONFIG                        } from '@/core/config/validate.config';

export function IsDateString(validationOptions?: ValidationOptions) {

  return (target: any, propertyKey: string) => {
    validationOptions = {message: VALIDATE_CONFIG.MESSAGES.isDateString, ...validationOptions ?? {}};

    ClassValidatorIsString(undefined, validationOptions)(target, propertyKey);
  };
}
