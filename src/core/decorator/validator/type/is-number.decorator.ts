import { ValidationOptions                  } from 'class-validator';
import { IsNumber as ClassValidatorIsNumber } from 'class-validator';

import { VALIDATE_CONFIG                    } from '@/core/config/validate.config';
import { IsNumberOptions                    } from 'class-validator/types/decorator/typechecker/IsNumber';

export function IsNumber(options?: IsNumberOptions, validationOptions?: ValidationOptions) {

  return (target: any, propertyKey: string) => {
    validationOptions = {message: VALIDATE_CONFIG.MESSAGES.isNumber, ...validationOptions ?? {}};

    ClassValidatorIsNumber(options, validationOptions)(target, propertyKey);
  };
}
