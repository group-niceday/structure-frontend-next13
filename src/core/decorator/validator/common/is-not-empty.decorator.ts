import { ValidationOptions                     } from 'class-validator';
import {IsNotEmpty as ClassValidatorIsNotEmpty } from 'class-validator';

import {VALIDATE_CONFIG} from '@/core/config/validate.config';

export function IsNotEmpty(validationOptions?: ValidationOptions) {

  return (target: any, propertyKey: string) => {
    validationOptions = {message: VALIDATE_CONFIG.MESSAGES.isNotEmpty, ...validationOptions ?? {}};

    ClassValidatorIsNotEmpty(validationOptions)(target, propertyKey);
  };
}
