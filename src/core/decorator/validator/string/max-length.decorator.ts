import { ValidationOptions              } from 'class-validator';
import { MaxLength as ClassValidatorMax } from 'class-validator';

import { VALIDATE_CONFIG                } from '@/core/config/validate.config';

export function MaxLength(maxValue: number, validationOptions?: ValidationOptions) {

  return (target: any, propertyKey: string) => {
    validationOptions = {message: VALIDATE_CONFIG.MESSAGES.maxLength, ...validationOptions ?? {}};

    ClassValidatorMax(maxValue, validationOptions)(target, propertyKey);
  };
}
