import { ValidationOptions              } from 'class-validator';
import { MinLength as ClassValidatorMin } from 'class-validator';

import { VALIDATE_CONFIG                } from '@/core/config/validate.config';

export function MinLength(minValue: number, validationOptions?: ValidationOptions) {

  return (target: any, propertyKey: string) => {
    validationOptions = {message: VALIDATE_CONFIG.MESSAGES.minLength, ...validationOptions ?? {}};

    ClassValidatorMin(minValue, validationOptions)(target, propertyKey);
  };
}
