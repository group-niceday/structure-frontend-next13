import { ValidationOptions, Validator } from 'class-validator';
import { ValidationError  }           from 'class-validator';

import   useValidateService     from '@/core/service/validate.service';
import   Container           from '@/core/container';
import   NotificationService from '@/app/shared/service/notification.service';
import   MESSAGE             from '@/app/shared/config/message.config';
import { NotificationEnum }  from '@/app/shared/enum/notification.enum';

const notificationService: NotificationService = Container.resolve(NotificationService);

const useValidate =  (target: any, propertyKey: string, descriptor: PropertyDescriptor, validatorOptions?: ValidationOptions) => {

  const { setErrorMessages } = useValidateService();
  const validator = new Validator();
  const original  = descriptor.value;

  descriptor.value = function (...args: any[]) {
    const errors: ValidationError[] = [];

    args.forEach((arg: any) => {
      errors.push(...validator.validateSync(arg, validatorOptions));
    });

    if (!!errors && errors.length > 0) {
      notificationService.onNotification(notificationService.setConfig(NotificationEnum.TYPE.warning, MESSAGE.NOTIFY.TITLE, setErrorMessages(errors)));

      return Promise.reject(new Error('CLASS_VALIDATOR_ERROR'));
    }

    return original.apply(this, args);
  };
};

export default useValidate;
