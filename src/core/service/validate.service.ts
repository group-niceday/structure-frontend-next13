import { ValidationError } from 'class-validator';

import { ValidateEnum    } from '@/core/config/validate.config';
import { ValidateConfig  } from '@/core/config/validate.config';
import { VALIDATE_CONFIG } from '@/core/config/validate.config';

const useValidateService = () => {

  const validateConfig: ValidateConfig = VALIDATE_CONFIG;

  const setDescriptions = (descriptions: any[]): string => {

    let returnValue: string = '';

    descriptions.forEach((description: string, index: number) => {
      if (index === 0) {
        returnValue = description;
      } else {
        returnValue = `${returnValue} - ${description}`;
      }
    });

    return returnValue;
  };

  const orderByConstraints = (constraints: string[]): string[] => {

    const returnValue: string[] = [];

    validateConfig.ORDER.forEach((order: ValidateEnum) => {
      if (constraints.indexOf(order) >= 0) {
        returnValue.push(order);
      }
    });

    return returnValue;
  };

  const setErrorMessages = (errors: ValidationError[], messages: string[] = [], parentDescription?: any[]): string[] => {

    errors.forEach((error: ValidationError) => {
      let   description: string   = '';
      const constraints: string[] = orderByConstraints(Object.keys((error.constraints as object) ?? {}));

      if (!!error.target) {
        description = Reflect.getMetadata('$description', error.target, error.property);
      }

      const descriptions: any = !!parentDescription ? Object.assign([], parentDescription) : [];

      if (!!description) {
        if (!!descriptions) {
          descriptions.push(description);
        }
      }

      if (!!constraints && constraints.length > 0) {
        messages.push(`${setDescriptions(descriptions)} : ${!!error.constraints ? error.constraints[constraints[0]] : ''}`);
      }

      if (!!error.children && error.children.length > 0) {
        messages = setErrorMessages(error.children, messages, descriptions);
      }
    });

    return messages;
  };

  return {
    setDescriptions,
    orderByConstraints,
    setErrorMessages,
  };
};

export default useValidateService;
