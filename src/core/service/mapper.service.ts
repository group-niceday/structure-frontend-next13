import { ClassConstructor } from 'class-transformer';
import { instanceToPlain  } from 'class-transformer';
import { plainToInstance  } from 'class-transformer';

// import { Pageable         } from '@/app/common/model/pageable.model';

const useMapper = () => {

  const toObject = <T>(type: ClassConstructor<T>, source: T): T => plainToInstance(type, source, { excludeExtraneousValues: true, exposeDefaultValues: true });
  const toArray = <T>(type: ClassConstructor<T>, source: T[]): T[] => plainToInstance(type, source, { excludeExtraneousValues: true, exposeDefaultValues: true });

  // toPage<T>(type: ClassConstructor<T>, source: Pageable.Response.Page<T>): Pageable.Response.Page<T> {
  //
  //   const page = plainToInstance(Pageable.Response.Page, source, {excludeExtraneousValues: true}) as Pageable.Response.Page<T>;
  //
  //   page.content = plainToInstance(type, source.content, {excludeExtraneousValues: true}) as T[];
  //
  //   return page;
  // }

  const toPlain = <T>(source: T): object => instanceToPlain(source);

  return {
    toObject,
    toArray,
    // toPage,
    toPlain,
  };
};

export default useMapper;
