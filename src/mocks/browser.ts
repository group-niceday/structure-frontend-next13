import { SetupWorker   } from 'msw';
import { setupWorker   } from 'msw';
import { artistMockApi } from '@/app/domain/artist/mock/artist-mock.api';

export const worker: SetupWorker = setupWorker(
  ...artistMockApi,
);
