import { SetupServer   } from 'msw/node';
import { setupServer   } from 'msw/node';
import { artistMockApi } from '@/app/domain/artist/mock/artist-mock.api';

export const server: SetupServer = setupServer(
  ...artistMockApi,
);
