import type { AppProps       } from 'next/app';
import      { ChakraProvider } from '@chakra-ui/react';

const MyApp = ({ Component, pageProps }: AppProps) => {

  const isServer = typeof window === 'undefined';

  if (process.env.NEXT_PUBLIC_NODE_ENV === 'local') {
    if (isServer) {
      (async () => {
        const { server } = await import('@/mocks/server');
        server.listen();
      })();
    } else {
      (async () => {
        const { worker } = await import('@/mocks/browser');
        await worker.start();
      })();
    }
  }

  return (
    <ChakraProvider>
      <Component {...pageProps} />
    </ChakraProvider>
  );
};

export default MyApp;
