import   React            from 'react';
import   axios            from 'axios';
import { NextRouter     } from 'next/router';
import { useRouter      } from 'next/router';
import { useEffect      } from 'react';
import { useState       } from 'react';

import   ArtistListView   from '@/app/view/artist/artist-list-view';
import { Artist         } from '@/app/domain/artist/model/artist.model';

const ArtistListPage = () => {

  const router: NextRouter        = useRouter();
  const [ lists,     setLists    ] = useState<Artist.Response.FindAll[]>([]);
  const [ errorMsg, setErrorMsg ] = useState('');

  const onAdd = () => {

    router.push('/artist/add');
  };

  const onRow = (id: number) => {

    router.push(`/artist/detail/${id}`);
  };

  useEffect(() => {
    axios.get('https://dev.niceday.io/api/artists/page')
      .then(response => {

        setLists(response.data.content);
      })
      .catch(() => {

        setErrorMsg('에러');
      });
  }, []);

  const props = {
    lists,
    errorMsg,
    onRow: id => onRow(id),
    onAdd: () => onAdd(),
  };

  return (
    <ArtistListView {...props} />
  );
};

export default ArtistListPage;
