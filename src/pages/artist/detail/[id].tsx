import   React              from 'react';
import   axios              from 'axios';
import { NextRouter       } from 'next/router';
import { useRouter        } from 'next/router';
import { useEffect        } from 'react';
import { useState         } from 'react';

import   ArtistDetailView   from '@/app/view/artist/artist-detail-view';
import { Artist           } from '@/app/domain/artist/model/artist.model';

const ArtistDetailPage = ()  => {

  const router: NextRouter    = useRouter();
  const [ detail, setDetail ] = useState<Artist.Response.FindOne>();
  const { id } = router.query;

  useEffect(() => {

    if (id) {
      axios.get(`https://dev.niceday.io/api/artists/${id}`)
        .then(response => {

          setDetail(response.data);
        });
    }
  }, [id]);

  const onDelete = () => {

    axios.delete(`https://dev.niceday.io/api/artists/${id}`)
      .then(response => response.data)
      .then(() => router.push('/artist'));
  };

  const onModify = () => {

    router.push(`/artist/modify/${id}`);
  };

  const onList = () => {

    router.push('/artist');
  };

  const props = {
    detail,
    onDelete: () => onDelete(),
    onModify: () => onModify(),
    onList: () => onList(),
  };

  return (
      <ArtistDetailView {...props} />
  );
};

export default ArtistDetailPage;
