import   React                    from 'react';
import   * as Sentry              from '@sentry/nextjs';
import   axios                    from 'axios';
import { NextRouter             } from 'next/router';
import { useRouter              } from 'next/router';
import { useToast               } from '@chakra-ui/react';
import { useForm                } from 'react-hook-form';

import   useMapper                from '@/core/service/mapper.service';
import   ArtistAddView            from '@/app/view/artist/artist-add-view';
import { Artist                 } from '@/app/domain/artist/model/artist.model';
import { classValidatorResolver } from '@hookform/resolvers/class-validator';

const ArtistAddPage = () => {

  const router: NextRouter = useRouter();
  const toast  = useToast();
  const mapper = useMapper();
  const resolver = classValidatorResolver(Artist.Request.Add);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<Artist.Request.Add>({ resolver, mode:'onChange' });
  const onToast = e => toast({

    title: e.message,
    status: 'error',
  });

  const onAdd = (add: Artist.Request.Add) => {

    axios.post('https://dev.niceday.io/api/artists', mapper.toObject(Artist.Request.Add, add))
      .then(response => response.data)
      .then(() => router.push('/artist'))
      .catch(e => {

        Sentry.captureException(e);
        return onToast(e);
      });
  };

  const onError = () => {

    console.log('error');
  };

  const props = {
    register: register,
    errors: errors,
    handleSubmit: () => handleSubmit(onAdd, onError),
  };

  return (
    <ArtistAddView {...props} />
  );
};

export default ArtistAddPage;
