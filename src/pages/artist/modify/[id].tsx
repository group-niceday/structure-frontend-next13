import   React                    from 'react';
import   axios                    from 'axios';
import { NextRouter             } from 'next/router';
import { useRouter              } from 'next/router';
import { useToast               } from '@chakra-ui/react';

import   ArtistModifyView         from '@/app/view/artist/artist-modify-view';
import { Artist                 } from '@/app/domain/artist/model/artist.model';
import { classValidatorResolver } from '@hookform/resolvers/class-validator';
import { useForm                } from 'react-hook-form';

const ArtistModifyPage = () => {

  const router: NextRouter = useRouter();
  const toast          = useToast();
  const { id }     = router.query;
  const resolver = classValidatorResolver(Artist.Request.Modify);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<Artist.Request.Modify>({ resolver,
    mode: 'onChange',
    defaultValues: async () => axios.get(`https://dev.niceday.io/api/artists/${id}`)
      .then(response => response.data) });

  const onToast = e => toast({

    title: e.message,
    status: 'error',
  });

  const onModify = (modify: Artist.Request.Modify) => {

    axios.put(`https://dev.niceday.io/api/artists/${id}`, modify)
      .then(response => response.data)
      .then(() => router.push('/artist'))
      .catch(e => onToast(e));
  };
  const onError = () => {

    console.log('error');
  };

  const props = {
    register: register,
    errors: errors,
    handleSubmit: () => handleSubmit(onModify, onError),
  };

  return (
    <ArtistModifyView {...props} />
  );
};

export default ArtistModifyPage;
