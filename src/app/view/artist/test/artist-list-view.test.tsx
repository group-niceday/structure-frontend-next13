import   React            from 'react';
import { fireEvent      } from '@testing-library/react';
import { render         } from '@testing-library/react';
import { screen         } from '@testing-library/react';

import   ArtistListView   from '@/app/view/artist/artist-list-view';
import { data           } from '@/app/domain/artist/mock/artist-mock.data';

const errorMsg: string    = '에러';
const onRow   : jest.Mock = jest.fn();
const onAdd   : jest.Mock = jest.fn();

describe('<ArtistListView />', () => {

  test('lists 를 props 로 받아서 랜더링 (3개)', () => {

    render(<ArtistListView lists={data.content}
                           onRow={onRow}
                           errorMsg={null}
                           onAdd={onAdd} />);

    const list: HTMLElement[] = screen.getAllByRole('list');

    expect(list).toHaveLength(3);
  });

  test('row 클릭시 onRow 함수 1번 호출', () => {

    render(<ArtistListView lists={data.content}
                           onRow={onRow}
                           errorMsg={null}
                           onAdd={onAdd} />);

    const rowButton: HTMLElement[] = screen.getAllByRole('list');

    fireEvent.click(rowButton[0]);

    expect(onRow).toHaveBeenCalledTimes(1);
  });

  test('등록 클릭시 onAdd 함수 1번 호출', () => {

    render(<ArtistListView lists={data.content}
                           onRow={onRow}
                           errorMsg={null}
                           onAdd={onAdd} />);

    const rowButton: HTMLElement = screen.getByRole('button', { name: '등록' });

    fireEvent.click(rowButton);

    expect(onAdd).toHaveBeenCalledTimes(1);
  });

  test('에러 메세지가 있을 때 에러 메세지 노출', () => {

    render(<ArtistListView lists={data.content}
                           onRow={onRow}
                           errorMsg={errorMsg}
                           onAdd={onAdd} />);

    const error: HTMLElement = screen.getByText(errorMsg);

    expect(error).toBeInTheDocument();
  });
});
