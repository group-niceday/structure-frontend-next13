import   React              from 'react';
import { render           } from '@testing-library/react';
import { screen           } from '@testing-library/react';
import { fireEvent        } from '@testing-library/react';

import   ArtistModifyView   from '@/app/view/artist/artist-modify-view';

const handleSubmit: jest.Mock = jest.fn();
const register: jest.Mock = jest.fn();
const errors = {
  name : {
    message: '필수 값 입니다.',
  },
};

describe('<ArtistModifyView />',  () => {

  test('수정 버튼 클릭 시 handleSubmit 함수 1번 호출', () => {

    render(<ArtistModifyView register={register}
                             errors={errors}
                             handleSubmit={handleSubmit} />);

    const modifyButton: HTMLElement = screen.getByRole('button', { name: '수정' });

    fireEvent.click(modifyButton);

    expect(handleSubmit).toHaveBeenCalledTimes(1);
  });

  test('에러 메세지 노출', () => {

    render(<ArtistModifyView register={register}
                             errors={errors}
                             handleSubmit={handleSubmit} />);

    expect(screen.getByText(errors.name.message)).toBeInTheDocument();
  });
});
