import   React        from 'react';
import { render     } from '@testing-library/react';
import { fireEvent  } from '@testing-library/react';
import { screen     } from '@testing-library/react';

import   ArtistAddView from '@/app/view/artist/artist-add-view';

const handleSubmit: jest.Mock = jest.fn();
const register    : jest.Mock = jest.fn();
const errors = {
  name : {
    message: '필수 값 입니다.',
  },
};

describe('<artistAddView />', () => {

  test('등록 버튼 클릭 시 handleSubmit 함수 1번 호출', () => {

    render(<ArtistAddView register={register}
                          errors={errors}
                          handleSubmit={handleSubmit} />);

    const addButton: HTMLElement = screen.getByRole('button', { name: '등록' });

    fireEvent.click(addButton);

    expect(handleSubmit).toHaveBeenCalledTimes(1);
  });

  test('에러 메세지 노출', () => {

    render(<ArtistAddView register={register}
                          errors={errors}
                          handleSubmit={handleSubmit} />);

    expect(screen.getByText(errors.name.message)).toBeInTheDocument();
  });
});
