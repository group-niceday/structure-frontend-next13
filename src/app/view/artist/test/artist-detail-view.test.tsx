import   React              from 'react';
import { render           } from '@testing-library/react';
import { screen           } from '@testing-library/react';
import { fireEvent        } from '@testing-library/react';

import   ArtistDetailView   from '@/app/view/artist/artist-detail-view';
import { data             } from '@/app/domain/artist/mock/artist-mock.data';

const onList  : jest.Mock = jest.fn();
const onModify: jest.Mock = jest.fn();
const onDelete: jest.Mock = jest.fn(() => data.content.pop());

describe('<ArtistDetailView />', () => {

  test('페이지 진입시 detail 랜더링.', () => {

    render(<ArtistDetailView detail={data.content[0]}
                             onDelete={onDelete}
                             onModify={onModify}
                             onList={onList} />);

    const detail: HTMLElement = screen.getByRole('list');

    expect(detail).toHaveTextContent((data.content[0].id).toString());
    expect(detail).toHaveTextContent(data.content[0].name);
    expect(detail).toHaveTextContent(data.content[0].agencyName);
    expect(detail).toHaveTextContent(data.content[0].birthDate);
    expect(detail).toHaveTextContent(data.content[0].description);
    expect(detail).toHaveTextContent(data.content[0].nationalityType);
  });

  test('목록 버튼 클릭시 onList 함수 1번 호출', () => {

    render(<ArtistDetailView detail={data.content[0]}
                             onDelete={onDelete}
                             onModify={onModify}
                             onList={onList} />);

    const listButton: HTMLElement = screen.getByRole('button', { name: '목록' });
    fireEvent.click(listButton);

    expect(onList).toHaveBeenCalledTimes(1);
  });

  test('수정 버튼 클릭시 onModify 함수 1번 호출', () => {

    render(<ArtistDetailView detail={data.content[0]}
                             onDelete={onDelete}
                             onModify={onModify}
                             onList={onList} />);

    const modifyButton: HTMLElement = screen.getByRole('button', { name: '수정' });
    fireEvent.click(modifyButton);

    expect(onModify).toHaveBeenCalledTimes(1);
  });

  test('삭제 버튼 클릭시 onDelete 함수 1번 호출', () => {

    render(<ArtistDetailView detail={data.content[0]}
                             onDelete={onDelete}
                             onModify={onModify}
                             onList={onList} />);

    const deleteButton: HTMLElement = screen.getByTestId('delete-button');

    fireEvent.click(deleteButton);

    expect(onDelete).toHaveBeenCalledTimes(1);
  });
});
