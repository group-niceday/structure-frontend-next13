import   React            from 'react';
import { Button         } from '@chakra-ui/react';
import { Center         } from '@chakra-ui/react';
import { TableContainer } from '@chakra-ui/react';
import { Table          } from '@chakra-ui/react';
import { Thead          } from '@chakra-ui/react';
import { Tbody          } from '@chakra-ui/react';
import { Td             } from '@chakra-ui/react';
import { Th             } from '@chakra-ui/react';
import { Tr             } from '@chakra-ui/react';
import { Box            } from '@chakra-ui/react';

type Props = {
  lists: any,
  errorMsg: any,
  onRow: any,
  onAdd: any,
};

const ArtistListView = ({
  lists,
  errorMsg,
  onRow,
  onAdd,
}: Props) => (
  errorMsg
    ? <span>{errorMsg}</span>
    : <>
      <Box borderWidth="1px" borderRadius="lg" overflow="hidden" p="6" m="6">
      <TableContainer>
          <Table variant="simple">
            <Thead>
              <Tr>
                <Th>아이디</Th>
                <Th>이름</Th>
                <Th>소속사명</Th>
                <Th>생일</Th>
                <Th>설명</Th>
                <Th>국적</Th>
              </Tr>
            </Thead>
            <Tbody>
              {lists.map(list => (
                <Tr key={list.id} role="list" onClick={() => onRow(list.id)} >
                  <Td>
                    {list.id}
                  </Td>
                  <Td>
                    {list.name}
                  </Td>
                  <Td>
                    {list.agencyName}
                  </Td>
                  <Td>
                    {list.birthDate}
                  </Td>
                  <Td>
                    {list.description}
                  </Td>
                  <Td>
                    {list.nationalityType}
                  </Td>
                </Tr>
              ))}
            </Tbody>
          </Table>
      </TableContainer>
      </Box>

      <Center>
        <Button width={100} colorScheme="blue" onClick={() => onAdd()}>등록</Button>
      </Center>
      </>
);

export default ArtistListView;
