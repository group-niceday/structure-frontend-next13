import   React    from 'react';
import { Button } from '@chakra-ui/react';
import { Input  } from '@chakra-ui/react';
import { Select } from '@chakra-ui/react';
import { Box    } from '@chakra-ui/react';
import { Center } from '@chakra-ui/react';

type Props = {
  register: any,
  errors: any,
  handleSubmit: any
};

const ArtistAddView = ({
  register,
  errors,
  handleSubmit,
}: Props) => (
      <Box m="6" borderWidth="1px" borderRadius="lg" overflow="hidden" p="6">
        <form onSubmit={handleSubmit()} >

        <label htmlFor="name">이름</label>
        <Input type="text"
               id="name"
               {...register('name')} />
          {errors.name && <span>{errors.name.message}</span>}

        <label htmlFor="agencyName">소속사명</label>
        <Input type="text"
               id="agencyName"
               {...register('agencyName')} />
          {errors.agencyName && <span>{errors.agencyName.message}</span>}

        <label htmlFor="birthDate">생일</label>
        <Input type="text"
               id="birthDate"
               {...register('birthDate')} />
          {errors.birthDate && <span>{errors.birthDate.message}</span>}

        <label htmlFor="description">설명</label>
        <Input type="text"
               id="description"
               {...register('description')} />
          {errors.description && <span>{errors.description.message}</span>}

        <label htmlFor="nationalityType">국적</label>
        <Select {...register('nationalityType')} id="nationalityType" >
          <option value="REPUBLIC_OF_KOREA">대한민국</option>
          <option value="USA">미국</option>
          <option value="NORTH_KOREA">북한</option>
          <option value="JAPAN">일본</option>
        </Select>
          {errors.nationalityType && <span>{errors.nationalityType.message}</span>}

        <Center>
          <Button type="submit" width={100} >등록</Button>
        </Center>
        </form>
      </Box>
);

export default ArtistAddView;
