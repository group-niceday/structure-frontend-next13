import   React            from 'react';
import { Popover        } from '@chakra-ui/react';
import { PopoverTrigger } from '@chakra-ui/react';
import { PopoverHeader  } from '@chakra-ui/react';
import { PopoverContent } from '@chakra-ui/react';
import { PopoverBody    } from '@chakra-ui/react';
import { Button         } from '@chakra-ui/react';
import { Box            } from '@chakra-ui/react';
import { Center         } from '@chakra-ui/react';
import { FormLabel      } from '@chakra-ui/react';
import { List           } from '@chakra-ui/react';
import { ListItem       } from '@chakra-ui/react';

type Props = {
  detail: any,
  onDelete: any,
  onModify: any,
  onList: any,
};

const ArtistDetailView = ({
  detail,
  onDelete,
  onModify,
  onList,
}: Props) => (
   <>
     <Box borderWidth="1px" borderRadius="lg" overflow="hidden" p="6" m="6">
      <List>
        <ListItem >
          <FormLabel>아이디</FormLabel>
          {detail?.id}
        </ListItem>
        <ListItem>
          <FormLabel>이름</FormLabel>
          {detail?.name}
        </ListItem>
        <ListItem>
          <FormLabel>소속사명</FormLabel>
          {detail?.agencyName}
        </ListItem>
        <ListItem>
          <FormLabel>생일</FormLabel>
          {detail?.birthDate}
        </ListItem>
        <ListItem>
          <FormLabel>설명</FormLabel>
          {detail?.description}
        </ListItem>
        <ListItem>
          <FormLabel>국가</FormLabel>
          {detail?.nationalityType}
        </ListItem>
      </List>
     </Box>

     <Center>
      <Button width={100} colorScheme="orange" onClick={() => onModify()}>
        수정
      </Button>

       <Popover>
        <PopoverTrigger>
          <Button width={100} colorScheme="red">삭제</Button>
        </PopoverTrigger>

        <PopoverContent>
          <PopoverHeader>삭제</PopoverHeader>

          <PopoverBody>
            삭제 하시겠습니까?
          </PopoverBody>

          <Button data-testid="delete-button" onClick={() => onDelete()}>
            삭제
          </Button>

        </PopoverContent>
       </Popover>

      <Button width={100}
              colorScheme="gray"
              onClick={() => onList()}>
        목록
      </Button>
     </Center>
   </>
);

export default ArtistDetailView;
