interface Data {
  content: {
    id             : number;
    name           : string;
    agencyName     : string;
    birthDate      : string;
    description    : string;
    nationalityType: string;
  }[];
}

export const data: Data = {
  content: [
    {
      id:1,
      name: '이름1',
      agencyName: '소속사명1',
      birthDate: '생일1',
      description: '설명1',
      nationalityType: 'REPUBLIC_OF_KOREA',
    },
    {
      id:2,
      name: '이름2',
      agencyName: '소속사명2',
      birthDate: '생일2',
      description: '설명2',
      nationalityType: 'REPUBLIC_OF_KOREA',
    },
    {
      id:3,
      name: '이름3',
      agencyName: '소속사명3',
      birthDate: '생일3',
      description: '설명3',
      nationalityType: 'REPUBLIC_OF_KOREA',
    },
  ],
};
