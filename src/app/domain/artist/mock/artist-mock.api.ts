import { rest } from 'msw';
import { data } from '@/app/domain/artist/mock/artist-mock.data';

export const artistMockApi = [

  rest.get(
    'https://dev.niceday.io/api/artists/page',
    (req, res, ctx) =>
      res(
        ctx.status(200),
        ctx.json(data),
      ),
  ),

  rest.get(
    'https://dev.niceday.io/api/artists/:id',
    (req, res, ctx) =>
      res(
        ctx.status(200),
        ctx.json(data.content[0]),
      ),
  ),

  rest.post(
    'https://dev.niceday.io/api/artists',
    (req, res, ctx) => res(
      ctx.status(200),
      ctx.json(data.content.push({
        id: 4,
        name: '이름4',
        agencyName: '소속사명4',
        birthDate: '생일4',
        description: '설명4',
        nationalityType: '국적4',
      })),
    ),
  ),

  rest.put(
    'https://dev.niceday.io/api/artists/:id',
    (req, res, ctx) => res(
      ctx.status(200),
      ctx.json(data.content.pop()),
    ),
  ),

  rest.delete(
    'https://dev.niceday.io/api/artists/:id',
    (req, res, ctx) => res(
      ctx.status(200),
      ctx.json(data.content.pop()),
    ),
  ),
];
