import                     'reflect-metadata';
import { Expose     } from 'class-transformer';

import { ArtistEnum } from '@/app/domain/artist/enum/artist.enum';
import { Attribute  } from '@/core/decorator/attribute.decorator';
import { IsString   } from '@/core/decorator/validator';
import { IsNotEmpty } from '@/core/decorator/validator';

export namespace Artist {
  export namespace Request {
    export class Add {
      @Expose()
      @IsString() @IsNotEmpty()
        name!: string;

      @Expose()
      @IsString() @IsNotEmpty()
        agencyName!: string;

      @Expose()
      @IsString() @IsNotEmpty()
        birthDate!: string;

      @Attribute('설명')
      @IsString() @IsNotEmpty()
        description!: string;

      @Attribute('국적')
      @IsString() @IsNotEmpty()
        nationalityType!: ArtistEnum.NATIONALITY_TYPE;
    }

    export class Modify {
      @Attribute('아티스트아이디')
        id!: number;

      @Attribute('이름')
      @IsString() @IsNotEmpty()
        name!: string;

      @Attribute('소속사명')
      @IsString() @IsNotEmpty()
        agencyName!: string;

      @Attribute('생일')
      @IsString() @IsNotEmpty()
        birthDate!: string;

      @Attribute('설명')
      @IsString() @IsNotEmpty()
        description!: string;

      @Attribute('국적')
      @IsString() @IsNotEmpty()
        nationalityType!: ArtistEnum.NATIONALITY_TYPE;
    }
  }

  export namespace Response {
    export class FindAll {
      @Attribute('아이디')
        id!: number;

      @Attribute('이름')
        name!: string;

      @Attribute('소속사명')
        agencyName!: string;

      @Attribute('생일')
        birthDate!: string;

      @Attribute('설명')
        description!: string;

      @Attribute('국적')
        nationalityType!: ArtistEnum.NATIONALITY_TYPE;
    }

    export class FindOne {
      @Attribute('아이디')
        id!: number;

      @Attribute('이름')
        name!: string;

      @Attribute('생일')
        birthDate!: string;

      @Attribute('소속사명')
        agencyName!: string;

      @Attribute('설명')
        description!: string;

      @Attribute('국적')
        nationalityType!: ArtistEnum.NATIONALITY_TYPE;
    }
  }
}
