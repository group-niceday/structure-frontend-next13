export namespace ArtistEnum {
  export enum NATIONALITY_TYPE {
    REPUBLIC_OF_KOREA = '대한민국',
    USA = '미국',
    NORTH_KOREA = '북한',
    JAPAN = '일본',
  }
}
